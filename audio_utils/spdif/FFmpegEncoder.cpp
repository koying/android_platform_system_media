/*
 *      Copyright (C) 2010-2013 Team XBMC
 *      http://xbmc.org
 *
 *  This Program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This Program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with XBMC; see the file COPYING.  If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 */
#define LOG_TAG "FFmpegEncoder"
#define AC3_ENCODE_BITRATE 640000

#include <utils/Log.h>

#include "FFmpegEncoder.h"

extern "C" {
#include <libavutil/avutil.h>
}

#include <cassert>
#include <string>

void ff_avutil_log(void* ptr, int level, const char* format, va_list va)
{
  std::string fmt_str(format);
  int final_n, n = ((int)fmt_str.size()) * 2; /* Reserve two times as much as the length of the fmt_str */
  std::unique_ptr<char[]> formatted;
  va_list argCopy;
  while(1)
  {
    formatted.reset(new char[n]); /* Wrap the plain char array into the unique_ptr */
    strcpy(&formatted[0], fmt_str.c_str());

    va_copy(argCopy, va);
    final_n = vsnprintf(&formatted[0], n, fmt_str.c_str(), argCopy);
    va_end(argCopy);
    if (final_n < 0 || final_n >= n)
      n += abs(final_n - n + 1);
    else
      break;
  }
  std::string message = std::string(formatted.get());

  std::string prefix;
  AVClass* avc= ptr ? *(AVClass**)ptr : NULL;
  if (avc)
  {
    if (avc->item_name)
      prefix += std::string("[") + avc->item_name(ptr) + "] ";
    else if (avc->class_name)
      prefix += std::string("[") + avc->class_name + "] ";
  }

  message = prefix + " " + message;
  switch (level)
  {
    case AV_LOG_INFO:
      ALOGI("%s", message.c_str());
      break;

    case AV_LOG_ERROR:
      ALOGE("%s", message.c_str());
      break;

    case AV_LOG_DEBUG:
    default:
      ALOGD("%s", message.c_str());
      break;
  }

}

CFFmpegEncoder::CFFmpegEncoder():
  m_inCodecID     (AV_CODEC_ID_NONE),
  m_inCodecCtx    (nullptr),
  m_frameBuffer   (nullptr),
  m_frameBufferPlanes (nullptr),
  m_outCodecID    (AV_CODEC_ID_NONE),
  m_outCodecCtx   (nullptr),
  m_BitRate       (0    ),
  m_SampleRateMul (0.0  ),
  m_inError       (false)
{
  ALOGV("ctor");

  /* register all the codecs */
  avcodec_register_all();

  av_log_set_callback(ff_avutil_log);
}

CFFmpegEncoder::~CFFmpegEncoder()
{
  ALOGV("dtor");
  Reset();

  if (m_inFrame)
    av_frame_free(&m_inFrame);
  if (m_inCodecCtx)
    avcodec_free_context(&m_inCodecCtx);
  if (m_outCodecCtx)
    avcodec_free_context(&m_outCodecCtx);

  free(m_frameBufferPlanes);
  free(m_frameBuffer);
}

bool CFFmpegEncoder::Initialize(audio_format_t format, int channels, int sample_rate)
{
  Reset();

  switch(format)
  {
    case AUDIO_FORMAT_E_AC3:
      m_inCodecID = AV_CODEC_ID_EAC3 ;
      break;
    case AUDIO_FORMAT_DTS_HD:
    case AUDIO_FORMAT_DTS:
      m_inCodecID = AV_CODEC_ID_DTS;
      break;
    default:
      break;
  }

  if (m_inCodecID == AV_CODEC_ID_NONE)
  {
    ALOGE("unsupported format %d", format);
    return false;
  }

  AVCodec *dec = avcodec_find_decoder(m_inCodecID);
  if (!dec)
  {
    ALOGE("Could not find ffmpeg decoder for input 0x:q%x", m_inCodecID);
    return false;
  }

  m_inCodecCtx = avcodec_alloc_context3(dec);
  if (!m_inCodecCtx)
  {
    ALOGE("Failed to allocate the decoder context for input stream");
    return false;
  }

  m_inCodecCtx->debug_mv = 0;
  m_inCodecCtx->debug = 0;
  m_inCodecCtx->workaround_bugs = 1;
  m_inCodecCtx->idct_algo         = 0;
  m_inCodecCtx->skip_frame        = AVDISCARD_DEFAULT;
  m_inCodecCtx->skip_idct         = AVDISCARD_DEFAULT;
  m_inCodecCtx->skip_loop_filter  = AVDISCARD_DEFAULT;
  m_inCodecCtx->error_concealment = 3;

  m_inCodecCtx->extradata = NULL;
  m_inCodecCtx->extradata_size = 0;

  m_inCodecCtx->channels = channels;
  m_inCodecCtx->sample_rate = sample_rate;
  m_inCodecCtx->sample_fmt = AV_SAMPLE_FMT_FLTP;

  if (avcodec_open2(m_inCodecCtx, dec, NULL) < 0)
  {
    ALOGE("Open() Unable to open input codec %d", format);
    avcodec_free_context(&m_inCodecCtx);
    return false;
  }

  AVCodec *codec = NULL;
  m_outCodecID = AV_CODEC_ID_AC3;
  m_BitRate = AC3_ENCODE_BITRATE;

  codec = avcodec_find_encoder(m_outCodecID);
  /* check we got the codec */
  if (!codec)
  {
    ALOGE("Failed to find encoder codec %d", m_outCodecID);
    return false;
  }

  m_outCodecCtx = avcodec_alloc_context3(codec);
  if (!m_outCodecCtx)
  {
    ALOGE("Failed to allocate the encoder context");
    return false;
  }

  m_outCodecCtx->bit_rate = m_BitRate;
  m_outCodecCtx->sample_rate = 48000;
  m_outCodecCtx->channel_layout = AV_CH_LAYOUT_5POINT1_BACK;
  m_outCodecCtx->channels = 6;
  m_outCodecCtx->sample_fmt = m_inCodecCtx->sample_fmt;

  /* open the codec */
  int ret;
  if ((ret = avcodec_open2(m_outCodecCtx, codec, NULL)) < 0)
  {
    ALOGE("cannot open output codec %d", ret);
    avcodec_free_context(&m_inCodecCtx);
    avcodec_free_context(&m_outCodecCtx);
    return false;
  }
  m_frameBufferNeededFrames = m_outCodecCtx->frame_size;
  m_frameBufferByteSize = m_frameBufferNeededFrames * m_outCodecCtx->channels * av_get_bytes_per_sample(m_outCodecCtx->sample_fmt);
  m_frameBuffer = (uint8_t*) malloc(m_frameBufferByteSize);
  m_frameBufferFrameUsed = 0;

	m_frameBufferPlanes = (uint8_t**)calloc(m_outCodecCtx->channels, sizeof(uint8_t*));
	av_samples_fill_arrays(m_frameBufferPlanes, NULL, m_frameBuffer, m_outCodecCtx->channels, m_outCodecCtx->frame_size, m_outCodecCtx->sample_fmt, 1);

  m_SampleRateMul = 1.0 / (double)m_outCodecCtx->sample_rate;

  m_inFrame = av_frame_alloc();
  if (!m_inFrame)
  {
    ALOGE("cannot allocate input frame");
    avcodec_free_context(&m_inCodecCtx);
    avcodec_free_context(&m_outCodecCtx);
    return false;
  }

  m_inBufferSizeUsed = 0;
  m_inBufferToSkip = 0;
  return true;
}

void CFFmpegEncoder::Reset()
{
  m_frameBufferFrameUsed = 0;

  m_inBufferToSkip = 0;
  m_inBufferSizeUsed = 0;
  m_inError = false;
  m_gotFrame = 0;

  if (m_inCodecCtx)
    avcodec_flush_buffers(m_inCodecCtx);
  if (m_outCodecCtx)
    avcodec_flush_buffers(m_outCodecCtx);
}

unsigned int CFFmpegEncoder::GetBitRate()
{
  return m_BitRate;
}

AVCodecID CFFmpegEncoder::GetCodecID()
{
  return m_outCodecID;
}

int CFFmpegEncoder::Transcode(const int frame_size, const uint8_t *in, const int in_size, uint8_t *out, int out_size)
{
  int got_output;
  AVFrame *frame;

  if (!m_inCodecCtx || !m_outCodecCtx)
    return 0;

  m_inError = false;

  const uint8_t* buffer = in;
  int bufferSize = in_size;
  m_frameSizeBytes = frame_size;

  if (m_inBufferSizeUsed + bufferSize > m_inBufferSize)
  {
    ALOGE("buffer too small %d + %d", m_inBufferSizeUsed, bufferSize);
    return 0;
  }

  memcpy(m_inBuffer + m_inBufferSizeUsed, (uint8_t *)buffer, bufferSize);
  m_inBufferSizeUsed += bufferSize;

  if (m_inBufferSizeUsed < m_frameSizeBytes)
    return 0;

  int out_consumed = 0;
  int in_consumed = 0;

  bufferSize = m_inBufferSizeUsed;
  buffer = m_inBuffer;

  while(bufferSize - in_consumed >= m_frameSizeBytes)
  {
    AVPacket avpkt;
    av_init_packet(&avpkt);
    avpkt.data = (uint8_t *)buffer + in_consumed;
    avpkt.size = m_frameSizeBytes;
    avpkt.dts = AV_NOPTS_VALUE;
    avpkt.pts = AV_NOPTS_VALUE;

    m_inFrame->decode_error_flags = 0;
    int consumed = avcodec_decode_audio4( m_inCodecCtx
                                                   , m_inFrame
                                                   , &m_gotFrame
                                                   , &avpkt);

    ALOGV(" consumed: %d; samples out= %d; gotframe: %d", consumed, m_inFrame->nb_samples, m_gotFrame);
    if (m_inFrame->decode_error_flags == FF_DECODE_ERROR_INVALID_BITSTREAM)  // Incomplete frame?
    {
      m_gotFrame = 0;
      break;
    }
    else if (consumed < 0)
    {
      ALOGE(" decoding error 0x%x", -consumed);
      Reset();
      m_inError = true;
      in_consumed += m_frameSizeBytes;
    }
    /* some codecs will attempt to consume more data than what we gave */
    else if (consumed > bufferSize - in_consumed)
    {
      ALOGW(" decoder attempted to consume more data than given %d", bufferSize - in_consumed);
      m_gotFrame = 0;
      in_consumed = m_frameSizeBytes;
    }
    else if (!m_gotFrame)
    {
      ALOGW(" no input frame found %d", consumed);
      in_consumed += consumed;
    }
    else
      in_consumed += consumed;

    if (m_gotFrame && !m_inError)
    {
      if (m_frameBufferFrameUsed + m_inFrame->nb_samples < m_frameBufferNeededFrames)
      {
        // TODO: not working in planar
    		av_samples_copy(m_frameBufferPlanes, m_inFrame->data, m_frameBufferFrameUsed, 0, m_inFrame->nb_samples, m_inCodecCtx->channels, m_inCodecCtx->sample_fmt);
        m_frameBufferFrameUsed += m_inFrame->nb_samples;
      }
      else
      {
        int framesToUse = std::min(m_frameBufferNeededFrames - m_frameBufferFrameUsed, m_inFrame->nb_samples);
    		av_samples_copy(m_frameBufferPlanes, m_inFrame->data, m_frameBufferFrameUsed, 0, framesToUse, m_inCodecCtx->channels, m_inCodecCtx->sample_fmt);

        /* allocate the input frame
         * sadly, we have to alloc/dealloc it everytime since we have no guarantee the
         * data argument will be constant over iterated calls and the frame needs to
         * setup pointers inside data */
        frame = av_frame_alloc();
        if (!frame)
        {
          ALOGE("Cannot allocate output frame");
          return 0;
        }

        frame->nb_samples = m_outCodecCtx->frame_size;
        frame->format = m_outCodecCtx->sample_fmt;
        frame->channel_layout = m_outCodecCtx->channel_layout;

        uint8_t *dec_data = m_frameBuffer;
        avcodec_fill_audio_frame(frame, m_outCodecCtx->channels, m_outCodecCtx->sample_fmt,
                          dec_data, m_frameBufferByteSize, 1);

        /* initialize the output packet */
        av_init_packet(&m_Pkt);
        m_Pkt.size = out_size - out_consumed;
        m_Pkt.data = out + out_consumed;

        /* encode it */
        int ret = avcodec_encode_audio2(m_outCodecCtx, &m_Pkt, frame, &got_output);

        /* free temporary data */
        av_frame_free(&frame);

        if (ret < 0 || !got_output)
        {
          ALOGE("Transcode - Encoding failed");
          in_consumed = bufferSize;
          break;
        }

        out_consumed += m_Pkt.size;

        /* free the packet */
        av_packet_unref(&m_Pkt);

        m_frameBufferFrameUsed = m_inFrame->nb_samples - framesToUse;
        if (m_frameBufferFrameUsed)
        {
          int frameSizeByte = av_frame_get_channels(m_inFrame) * av_get_bytes_per_sample(m_inCodecCtx->sample_fmt);
          memcpy(m_frameBuffer, m_inFrame->data[0] + (framesToUse * frameSizeByte), m_frameBufferFrameUsed * frameSizeByte);
        }
      }
    }
  }

  m_inBufferSizeUsed -= in_consumed;
  if (m_inBufferSizeUsed)
    memcpy(m_inBuffer, m_inBuffer + in_consumed, m_inBufferSizeUsed);

  /* return the number of byte used */
  return out_consumed;
}

bool CFFmpegEncoder::IsInBetweenFrame()
{
  return (m_inBufferSizeUsed > 0);
}

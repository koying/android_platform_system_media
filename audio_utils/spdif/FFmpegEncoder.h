#pragma once
/*
 *      Copyright (C) 2010-2013 Team XBMC
 *      http://xbmc.org
 *
 *  This Program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This Program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with XBMC; see the file COPYING.  If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 */
#include <system/audio.h>

extern "C" {
#include <libavcodec/avcodec.h>
}

/* ffmpeg re-defines this, so undef it to squash the warning */
#undef restrict

class CFFmpegEncoder
{
public:
  CFFmpegEncoder();
  ~CFFmpegEncoder();

  bool Initialize(audio_format_t format, int channels, int sample_rate);
  void Reset();

  unsigned int GetBitRate();
  AVCodecID GetCodecID();

  int Transcode(const int frameSizeBytes, const uint8_t *in, const int in_size, uint8_t *out, int out_size);
  bool IsInBetweenFrame();
  bool IsInError() { return m_inError; }

public:
  uint8_t m_inBuffer[65535];
  int m_inBufferSize = 65535;
  int m_inBufferSizeUsed;

private:
  AVFrame* m_inFrame;
  int m_gotFrame;

  AVCodecID m_inCodecID;
  AVCodecContext *m_inCodecCtx;
  int m_inBufferToSkip;

  int m_frameSizeBytes;
  int m_frameBufferFrameUsed;
  int m_frameBufferNeededFrames;
  int m_frameBufferByteSize;
  uint8_t* m_frameBuffer;
  uint8_t **m_frameBufferPlanes;

  AVCodecID m_outCodecID;
  AVCodecContext *m_outCodecCtx;

  unsigned int m_BitRate;
  AVPacket m_Pkt;
  double m_SampleRateMul;
  bool m_inError;
};

